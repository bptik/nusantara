﻿using UnityEngine;

namespace EasyUI.PickerWheelUI
{
    [System.Serializable]
    public class WheelPiece
    {
        public Sprite Icon;
        public string Label;
        public int Amount;
        public float Chance;
        public GameObject IndicatorPanel; // Panel yang akan diaktifkan saat roda berhenti di indikator ini

        [HideInInspector]
        public int Index;
        [HideInInspector]
        public double _weight;
    }
}
