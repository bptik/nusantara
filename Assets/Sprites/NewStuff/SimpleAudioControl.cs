using System.Collections;
using System.Collections.Generic;
using Herghys.SimpleAudioManager;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public class SimpleAudioControl : MonoBehaviour
    {
        public bool isBGMAudioEnabled = true;
        public Image on;
        public Image off;

        private void Awake()
        {
            if (AudioManager.Instance != null)
            AudioManager.Instance.ControlBGM(isBGMAudioEnabled);
            ControlImage();
        }

        public void ToggleAudio()
        {
            if (AudioManager.Instance == null) return;
				isBGMAudioEnabled = !isBGMAudioEnabled;
            ControlImage();
            AudioManager.Instance.ControlBGM(isBGMAudioEnabled);
        }

        private void ControlImage()
        {
            if (isBGMAudioEnabled) 
            {
                on.gameObject.SetActive(true);
                off.gameObject.SetActive(false);
            }
            else
            {
				on.gameObject.SetActive(false);
				off.gameObject.SetActive(true);
			}
        }
    }
}
