using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Herghys.Global;
using Herghys.SimpleAudioManager;
using Redcode.Awaiting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class MainMenu : MonoBehaviour
    {
      
		AsyncOperation op = new AsyncOperation();
		[SerializeField] SceneFader fader;

        [SerializeField] Button nextButton;

        private void Awake()
        {
            fader.FadeOut();
            GlobalData.CharacterSelected = false;
        }

        private void OnEnable()
        {
            nextButton.onClick.AddListener(NextScene);
        }
        private void OnDisable()
        {
            nextButton.onClick.RemoveListener(NextScene);
        }

        public void NextScene()
        {
            LoadNextScene();
        }

        async void LoadNextScene()
        {
			AsyncOperation op= SceneManager.LoadSceneAsync("MissionSelection");
			op.allowSceneActivation = false;
			await fader.FadeInTask();
			DOTween.Clear(true);
			op.allowSceneActivation = true;
		}

        public void ResetData()
            => GlobalData.SetInitialValue();
    }
}
