using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollider2 : MonoBehaviour
{
   public GameObject popupPanel; // Panel atau pop-up yang akan ditampilkan saat pemain menabrak
    public GameObject replacementObject; // Objek pengganti setelah pemain menabrak

    private bool isCollisionHandled = false; // Status apakah tabrakan sudah ditangani atau belum
    private CharacterTopDown playerMovement; // Komponen pemain yang mengatur gerakan

    private void Start()
    {
        playerMovement = GetComponent<CharacterTopDown>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isCollisionHandled && collision.gameObject.CompareTag("Player"))
        {
            isCollisionHandled = true;

            // Menampilkan panel atau pop-up
            popupPanel.SetActive(true);

            // Mengubah objek menjadi objek pengganti
            Instantiate(replacementObject, transform.position, transform.rotation);
            Destroy(gameObject);

            // Menonaktifkan gerakan pemain
            playerMovement.enabled = false;
        }
    }
}
