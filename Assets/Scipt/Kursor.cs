using UnityEngine;

public class Kursor : MonoBehaviour
{
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    void Start()
    {
        // Mengubah tampilan kursor saat game dimulai
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
}
