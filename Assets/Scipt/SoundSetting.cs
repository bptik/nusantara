// using UnityEngine;

// public class SoundSetting : MonoBehaviour
// {
//     // Referensi ke instance SoundManagerClip
//       public static SoundManagerClip Instance { get; private set; }

//     private void Start()
//     {
//         // Cari instance SoundManagerClip di dalam scene
//         soundManager = SoundManagerClip.Instance;
//     }

//     public void OnPauseButtonClicked()
//     {
//         // Jeda permainan atau jeda fungsi permainan lainnya di sini

//         // Jeda pemutaran audio
//         if (soundManager != null && soundManager.soundSource != null && soundManager.soundSource.isPlaying)
//         {
//             soundManager.soundSource.Pause();
//         }
//     }

//     public void OnResumeButtonClicked()
//     {
//         // Lanjutkan permainan atau lanjutkan fungsi permainan lainnya di sini

//         // Lanjutkan pemutaran audio
//         if (soundManager != null && soundManager.soundSource != null && !soundManager.soundSource.isPlaying)
//         {
//             soundManager.soundSource.UnPause();
//         }
//     }

//     public void OnStopButtonClicked()
//     {
//         // Hentikan permainan atau hentikan fungsi permainan lainnya di sini

//         // Hentikan pemutaran audio
//         if (soundManager != null && soundManager.soundSource != null)
//         {
//             soundManager.soundSource.Stop();
//         }
//     }
// }
