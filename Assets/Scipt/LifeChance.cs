using UnityEngine;
using UnityEngine.UI;

public class LifeChance : MonoBehaviour
{
    public Image lifeImage;
    public Sprite spriteEmpty;
    public Sprite spriteFull;
    public GameObject gameOverPanel;

    private int lifeChance = 3; // Inisialisasi jumlah life chance awal

    void Start()
    {
        UpdateLifeChanceUI();
    }

    public void ReduceLifeChance()
    {
        // Kurangi life chance saat button diklik
        if (lifeChance > 0)
        {
            lifeChance--;
            UpdateLifeChanceUI();
        }

        // Tampilkan panel gameOverPanel jika life chance habis
        if (lifeChance == 0)
        {
            ShowGameOverPanel();
        }
    }

    void UpdateLifeChanceUI()
    {
        // Mengganti gambar life chance sesuai dengan jumlah life chance
        if (lifeChance > 0)
        {
            lifeImage.sprite = spriteFull; // Menggunakan sprite full jika life chance masih tersisa
        }
        else
        {
            lifeImage.sprite = spriteEmpty; // Menggunakan sprite empty jika life chance habis
        }
    }

    void ShowGameOverPanel()
    {
        // Aktifkan panel gameOverPanel
        if (gameOverPanel != null)
        {
            gameOverPanel.SetActive(true);

            // Optional: Jika menggunakan "Canvas Group" untuk panel, buat panel terlihat (Alpha = 1) dan interaktif (Interactable = true)
            // CanvasGroup canvasGroup = gameOverPanel.GetComponent<CanvasGroup>();
            // canvasGroup.alpha = 1;
            // canvasGroup.interactable = true;
        }
    }
}
