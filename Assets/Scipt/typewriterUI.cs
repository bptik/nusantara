using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class typewriterUI : MonoBehaviour
{
    Text _text;
    string writer;

    [SerializeField] float delayBeforeStart = 0f;
    float timeBtwChars = 0.06f;
    [SerializeField] string leadingChar = "";
    [SerializeField] bool leadingCharBeforeDelay = false;

    [SerializeField] GameObject NextButton;
    [SerializeField] GameObject SkipButton;

    void Start()
    {
        _text = GetComponent<Text>()!;

        if (_text != null)
        {
            writer = _text.text;
            _text.text = "";

            StartCoroutine("TypeWriterText");
        }
    }

    public void _skip()
    {
        timeBtwChars = 0;
        NextButton.SetActive(true);
        SkipButton.SetActive(false);
    }

    IEnumerator TypeWriterText()
    {
        _text.text = leadingCharBeforeDelay ? leadingChar : "";

        yield return new WaitForSeconds(delayBeforeStart);
        foreach (char c in writer)
        {
            if (_text.text.Length > 0)
            {
                _text.text = _text.text.Substring(0, _text.text.Length);
            }
            _text.text += c;
            _text.text += leadingChar;

            yield return new WaitForSeconds(timeBtwChars);
            //NextButton.SetActive(true);
            
        }
        NextButton.SetActive(true);
        SkipButton.SetActive(false);


        if (leadingChar != "")
        {
            _text.text = _text.text.Substring(0, _text.text.Length - leadingChar.Length);
        }
    }
}
