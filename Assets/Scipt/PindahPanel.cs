using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PindahPanel : MonoBehaviour
{
    public GameObject PanelAwal;
    public GameObject PanelLanjutan;
    public GameObject PanelKeluar;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PanelPengaturan()
    {
        PanelAwal.SetActive(false);
        PanelLanjutan.SetActive(true);

    }

    public void PanelExit()
    {
        PanelAwal.SetActive(false);
        PanelKeluar.SetActive(true);
    }

    public void PanelLevel1()
    {
        PanelAwal.SetActive(false);
        PanelLanjutan.SetActive(true);
    }

    public void PanelLevel2()
    {
        PanelAwal.SetActive(false);
        PanelLanjutan.SetActive(true);
    }

    public void PanelLevel3()
    {
        PanelAwal.SetActive(false);
        PanelLanjutan.SetActive(true);
    }

    public void PanelLevel4()
    {
        PanelAwal.SetActive(false);
        PanelLanjutan.SetActive(true);
    }

    public void PanelPetunjukLevel()
    {
        PanelAwal.SetActive(false);
        PanelLanjutan.SetActive(true);
    }
    public void PanelHome()
    {
        PanelAwal.SetActive(true);
        PanelLanjutan.SetActive(true);
    }
}
