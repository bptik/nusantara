using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenPanel : MonoBehaviour
{
    public  GameObject PanelAwal;
    public  GameObject PanelTujuan;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GantiPanel()
    {
        PanelAwal.SetActive(false);
        PanelTujuan.SetActive(true);
    }
}
