// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.UI;

// public class GameManager : MonoBehaviour
// {
//     private int totalSkor;
//     private int totalGold;

//     public Text textTotalSkor;
//     public Text textTotalGold;

//     private void Awake()
//     {
//         // Mengambil total skor dan total gold dari PlayerPrefs
//         totalSkor = PlayerPrefs.GetInt("TotalSkor", 0);
//         totalGold = PlayerPrefs.GetInt("TotalGold", 0);

//         // Update teks total skor dan total gold saat permainan dimulai
//         UpdateTotalSkorText();
//         UpdateTotalGoldText();
//     }

//     public void AddSkor(int skorToAdd)
//     {
//         totalSkor += skorToAdd;

//         // Update teks total skor setelah ditambahkan
//         UpdateTotalSkorText();
//     }

//     public void AddGold(int goldToAdd)
//     {
//         totalGold += goldToAdd;

//         // Update teks total gold setelah ditambahkan
//         UpdateTotalGoldText();
//     }

//     public int GetTotalSkor()
//     {
//         return totalSkor;
//     }

//     public int GetTotalGold()
//     {
//         return totalGold;
//     }

//     public void SaveTotalSkor()
//     {
//         // Menyimpan total skor ke PlayerPrefs
//         PlayerPrefs.SetInt("TotalSkor", totalSkor);
//         PlayerPrefs.Save();
//     }

//     public void SaveTotalGold()
//     {
//         // Menyimpan total gold ke PlayerPrefs
//         PlayerPrefs.SetInt("TotalGold", totalGold);
//         PlayerPrefs.Save();
//     }

//       public void ResetTotalSkor()
//     {
//         totalSkor = 0;

//         // Update teks total skor setelah direset
//         UpdateTotalSkorText();
//     }

//     public void ResetTotalGold()
//     {
//         totalGold = 0;

//         // Update teks total gold setelah direset
//         UpdateTotalGoldText();
//     }

//     private void UpdateTotalSkorText()
//     {
//         textTotalSkor.text = "Total Skor: " + totalSkor.ToString();
//     }

//     private void UpdateTotalGoldText()
//     {
//         textTotalGold.text = "Total Gold: " + totalGold.ToString();
//     }
// }
