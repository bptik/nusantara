using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectCollider : MonoBehaviour
{
 public GameObject ObjekBaru; // Prefab objek baru setelah terjadi tabrakan
    public GameObject PanelAktif;
    public GameObject PanelOff;
    public GameObject GajahMada;

    private bool telahDitabrak = false; // Menyimpan status apakah objek sudah ditabrak atau belum

    private void OnTriggerEnter2D(Collider2D menabrak)
    {
        if (menabrak.gameObject.CompareTag("Player") && !telahDitabrak)
        {
            telahDitabrak = true;

            PanelAktif.SetActive(true);
            PanelOff.SetActive(false);
            GajahMada.SetActive(false);

            // Menginstansiasi objek baru
            GameObject newObject = Instantiate(ObjekBaru, transform.position, transform.rotation);

            // Mengatur parent objek baru menjadi parent objek yang sebelumnya
            newObject.transform.parent = transform.parent;

            // Menonaktifkan collider pada objek ini agar tidak dapat ditabrak lagi
            GetComponent<Collider2D>().enabled = false;
        }
    }

}
