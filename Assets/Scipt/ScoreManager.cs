using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    private int TotalGold;
    // private int TotalScore;

    public GameObject star1; // Referensi ke GameObject bintang pertama
    public GameObject star2; // Referensi ke GameObject bintang kedua
    public GameObject star3; // Referensi ke GameObject bintang ketiga

    public Text goldText; // Referensi ke Text component untuk menampilkan total Gold
    // public Text scoreText; // Referensi ke Text component untuk menampilkan total score

    void Start()
    {
        // LoadScore();
        // UpdateTexts();
        UpdateStars();
        int gold1 = PlayerPrefs.GetInt("Gold1", 0);
        int gold2 = PlayerPrefs.GetInt("Gold2", 0);
        int gold3 = PlayerPrefs.GetInt("Gold3", 0);
        int gold4 = PlayerPrefs.GetInt("Gold4", 0);

        int TotalGold = gold1 + gold2;
        goldText.text = TotalGold.ToString();

        PlayerPrefs.SetInt("TotalGold", TotalGold);


    }

    // public void AddGold(int amount)
    // {
    //     totalGold += amount;
    //     SaveGold();
    //     UpdateGoldText();
    // }

    // public void AddScore(int amount)
    // {
    //     totalScore += amount;
    //     SaveScore();
    //     UpdateScoreText();
    // }

    // public void SaveGold()
    // {
    //     PlayerPrefs.SetInt("Gold", totalGold);
    //     PlayerPrefs.Save();
    // }

    // public void SaveScore()
    // {
    //     PlayerPrefs.SetInt("Skor", totalScore);
    //     PlayerPrefs.Save();
    // }

    // public void LoadScore()
    // {
    //     totalGold = PlayerPrefs.GetInt("Gold", 0);
    //     totalScore = PlayerPrefs.GetInt("Skor", 0);
    // }

    public int GetTotalGold()
    {
        return TotalGold;
    }

    // public int GetTotalScore()
    // {
    //     return TotalScore;
    // }

    // // Fungsi untuk memperbarui teks total Gold
    // private void UpdateGoldText()
    // {
    //     goldText.text = "Gold : " + totalGold.ToString();
    // }

    // // Fungsi untuk memperbarui teks total score
    // private void UpdateScoreText()
    // {
    //     scoreText.text = "Score : " + totalScore.ToString();
    // }

    // // Fungsi untuk memperbarui teks total Gold dan score
    // private void UpdateTexts()
    // {
    //     UpdateGoldText();
    //     UpdateScoreText();
    // }

    void update()
    {
        // int scoreLevel1 = PlayerPrefs.GetInt("Skor1");
        // int scoreLevel2 = PlayerPrefs.GetInt("Skor2");
        // int scoreLevel3 = PlayerPrefs.GetInt("Skor3");
        // int scoreLevel4 = PlayerPrefs.GetInt("Skor4");

        // int GoldLevel1 = PlayerPrefs.GetInt("Gold1");
        // int GoldLevel2 = PlayerPrefs.GetInt("Gold2");
        // int GoldLevel3 = PlayerPrefs.GetInt("Gold3");
        // int GoldLevel4 = PlayerPrefs.GetInt("Gold4");

        // // int TotalScore = scoreLevel1 + scoreLevel2 + scoreLevel3 + scoreLevel4;
        // int TotalGold = GoldLevel1 + GoldLevel2 + GoldLevel3 + GoldLevel4;

        // goldText.text = TotalGold.ToString();
        // // scoreText.text = TotalScore.ToString();
    }
    private void UpdateStars()
    {
        int TotalGold = GetTotalGold();

        star1.SetActive(TotalGold >= 1 && TotalGold < 330000);
        star2.SetActive(TotalGold >= 330000 && TotalGold < 360000);
        star3.SetActive(TotalGold >= 370000 && TotalGold <= 400000);
    }
}
