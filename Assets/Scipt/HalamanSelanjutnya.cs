using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HalamanSelanjutnya : MonoBehaviour
{
   public void PindahScene_StoryMaps1() //Pindah halaman ke story maps
    {
        SceneManager.LoadScene("Story_Maps");
    }

    public void PindahScene_StoryMaps2() //Pindah halaman ke stroy maps 2
    {
        SceneManager.LoadScene("Story_Maps 2");
    }

    public void PindahScene_StoryMaps3() //Pindah halaman ke stroy maps 2
    {
        SceneManager.LoadScene("Story_Maps 3");
    }

    public void PindahScene_Study()
    {
        SceneManager.LoadScene("Study");
    }
    public void PindahScene_Level1()
    {
        SceneManager.LoadScene("Level1");
    }
    public void PindahScene_Level2()
    {
        SceneManager.LoadScene("Level2");
    }
    public void PindahScene_Level3()
    {
        SceneManager.LoadScene("Level3");
    }
    public void PindahScene_Level4()
    {
        SceneManager.LoadScene("Level4");
    }

    public void PindahScene_ModulLevel1()
    {
        SceneManager.LoadScene("ModulLevel1");
    }

    public void PindahScene_ModulLevel2()
    {
        SceneManager.LoadScene("ModulLevel2");
    }

    public void PindahScene_ModulLevel3()
    {
        SceneManager.LoadScene("ModulLevel3");
    }
    public void PindahScene_ModulLevel4()
    {
        SceneManager.LoadScene("ModulLevel4");
    }

    public void PindahScene_Home()
    {
        SceneManager.LoadScene("Home");
    }

    public void PindahScene_MapsLevel()
    {
        SceneManager.LoadScene("MapsLevel");
    }

    public void PindahScene_PetunjukL1()
    {
        SceneManager.LoadScene("PetunjukLevel1");
    }

     public void PindahScene_PetunjukL2()
    {
        SceneManager.LoadScene("PetunjukLevel2");
    }

     public void PindahScene_PetunjukL3()
    {
        SceneManager.LoadScene("PetunjukLevel3");
    }
     public void PindahScene_PetunjukL4()
    {
        SceneManager.LoadScene("PetunjukLevel4");
    }

     public void PindahScene_TotalScore()
    {
        SceneManager.LoadScene("TotalScore");
    }

    public void PindahScene_Level4New()
    {
        SceneManager.LoadScene("Wheel");
    }

    
}
