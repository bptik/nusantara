using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTopDown : MonoBehaviour
{
  public float moveSpeed = 5f;  // Kecepatan karakter
    private Rigidbody2D rb;  // Komponen Rigidbody2D
    private Vector2 moveDirection;  // Arah gerakan karakter

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        ProcessInput();
    }

    void FixedUpdate()
    {
        Move();
    }

    void ProcessInput()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        moveDirection = new Vector2(moveX, moveY).normalized;
    }

    void Move()
    {
        rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);
    }
}
