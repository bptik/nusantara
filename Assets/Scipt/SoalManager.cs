using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoalManager : MonoBehaviour
{
    [System.Serializable]
    public class Soal
    {
        [TextArea]
        [Header("Soal")]
        public string soal;

        [Header("Gambar Soal")]
        public Sprite gambarSoal;

        [Header("Pilihan untuk jawaban")]
        public Sprite pilA;
        public Sprite pilB, pilC, pilD;

        [Header("Kunci Jawaban")]
        public bool A;
        public bool B, C, D;
    }

    public float waktuPerSoal;
    public float jawabanTimer; // Timer untuk setiap jawaban
    private int nilaiAcak;

    public Text textWaktu;
    public Text teksSkor;
    public Text teksHasilSkor;
    public Text teksLiveScore;
    public Text teksGold; // Teks untuk menampilkan poin emas
    public Image[] lifeChanceImages;
    public Sprite lifeChanceFullSprite;
    public Sprite lifeChanceEmptySprite;

    public GameObject gameOverPanel;
    public GameObject winPanel;

    public List<Soal> KumpulanSoal;
    private int skor = 0;
    private int lifeChances = 3;
    private int gold = 0;

    private bool isGameFinished = false;
    private bool isAnswerGiven = false;
    private int answerIndex1 = -1;
    private int answerIndex2 = -1;

    private float initialTime;
    private float jawabanTimeLeft; // Waktu yang tersisa untuk menjawab

    public AudioClip soundJawabanBenar;
    public AudioClip soundJawabanSalah;
    public AudioClip soundTimeOut;
    // private AudioSource audioSource;

    public int goldAmount = 20000;
    private bool isTimerRunning = false;

    public Text textSoal;
    public Image gambarSoal;
    public Image imageA, imageB, imageC, imageD;

    private void Start()
    {
        teksLiveScore.text = "Score : 0";
        SetLifeChanceSprites();
        initialTime = waktuPerSoal;
        jawabanTimeLeft = jawabanTimer; // Set waktu jawaban awal
        int skor = PlayerPrefs.GetInt("Skor", 0);
        int goldAmount = PlayerPrefs.GetInt("Gold3", 0);
        // audioSource = GetComponent<AudioSource>();

        
        teksGold.text = "Gold : " + gold.ToString();

        // Mengacak indeks soal
        ShuffleSoal();
        nilaiAcak = 0; // Mulai dengan indeks pertama

        UpdateSoal();
    }

    private void Update()
    {
        if (isGameFinished)
        {
            return;
        }

        textWaktu.text = "Time : " + waktuPerSoal.ToString("0.0");

        if (isTimerRunning)
        {
            waktuPerSoal -= Time.deltaTime;

            if (waktuPerSoal <= 0)
            {
                if (!isAnswerGiven)
                {
                    ReduceLifeChance();
                }

                // PlaySound(soundTimeOut);
                ResetAnswerFlags();
                isTimerRunning = false; // Memberhentikan timer setelah waktu habis

                // Melanjutkan ke pertanyaan berikutnya
                NextQuestion();
            }
        }

        if (isAnswerGiven)
        {
            jawabanTimeLeft -= Time.deltaTime; // Mengurangi waktu jawaban

            if (jawabanTimeLeft <= 0)
            {
                ReduceLifeChance();
                // PlaySound(soundTimeOut);
                ResetAnswerFlags();
                isTimerRunning = false; // Memberhentikan timer setelah waktu jawaban habis
            }
        }
    }

    public void CekJawaban(int index)
    {
        if (isGameFinished)
        {
            return;
        }

        if (!isAnswerGiven)
        {
            if (answerIndex1 == -1)
            {
                answerIndex1 = index;
            }
            else if (answerIndex2 == -1)
            {
                answerIndex2 = index;
                isAnswerGiven = true;
                isTimerRunning = false; // Memberhentikan timer saat menjawab pertanyaan
            }
        }

        if (isAnswerGiven)
        {
            // Mendapatkan pilihan jawaban yang benar
            Soal currentSoal = KumpulanSoal[nilaiAcak];
            bool kunciJawaban1 = GetAnswerByIndex(currentSoal, answerIndex1);
            bool kunciJawaban2 = GetAnswerByIndex(currentSoal, answerIndex2);

            // Memeriksa jawaban yang diberikan
            if (kunciJawaban1)
            {
                skor++;
                // audioSource.PlayOneShot(soundJawabanBenar);
                GiveGold();
            }

            if (kunciJawaban2)
            {
                skor++;
                // audioSource.PlayOneShot(soundJawabanBenar);
                GiveGold();
            }

            ResetAnswerFlags();

            if (!kunciJawaban1 || !kunciJawaban2)
            {
                ReduceLifeChance();

                if (lifeChances < 1)
                {
                    isGameFinished = true;
                    Debug.Log("No more life chances. Game over.");
                    ShowGameOverPanel();
                    teksHasilSkor.gameObject.SetActive(true);
                    teksLiveScore.gameObject.SetActive(false);
                    return;
                }
                else
                {
                    Debug.Log("Incorrect answer. Remaining life chances: " + lifeChances);
                    UpdateLifeChanceSprites();
                }
            }

            KumpulanSoal.RemoveAt(nilaiAcak);
            if (KumpulanSoal.Count > 0)
            {
                nilaiAcak = 0; // Mulai dengan indeks pertama
                UpdateSoal();
                isTimerRunning = true; // Memulai timer untuk pertanyaan berikutnya
            }
            else
            {
                teksSkor.text = "Skor : " + skor.ToString();

                if (skor < 7)
                {
                    ShowGameOverPanel();
                    GiveGold();
                }
                else if (skor >= 8)
                {
                    ShowWinPanel();
                    GiveGold();
                    UnlockNewLevel();
                }

                isGameFinished = true;
                Debug.Log("All questions answered. Time stopped.");

                teksHasilSkor.text = "Your Score: " + skor.ToString();
                teksHasilSkor.gameObject.SetActive(true);
                teksLiveScore.gameObject.SetActive(false);

                if (isGameFinished)
                {
                    GiveGold();
                }
            }
        }

        ResetTimer();
        teksLiveScore.text = "Skor : " + skor.ToString();
    }

    public void SkipJawaban()
    {
        if (isGameFinished || isAnswerGiven)
        {
            return;
        }

        if (!isAnswerGiven)
        {
            ReduceLifeChance();
            UpdateLifeChanceSprites(); // Memperbarui tampilan sprite peluang hidup
            ResetTimer();
            isTimerRunning = true; // Memulai timer untuk pertanyaan berikutnya
        }

        KumpulanSoal.RemoveAt(nilaiAcak);
        waktuPerSoal = initialTime;

        if (KumpulanSoal.Count > 0)
        {
            nilaiAcak = 0; // Mulai dengan indeks pertama
            UpdateSoal();
        }
        else
        {
            teksSkor.text = "Skor : " + skor.ToString();

            if (skor < 7)
            {
                ShowGameOverPanel();
                GiveGold();
            }
            else if (skor >= 8)
            {
                ShowWinPanel();
                GiveGold();
                UnlockNewLevel();
            }

            isGameFinished = true;
            Debug.Log("All questions answered. Time stopped.");

            teksHasilSkor.text = "Your Score: " + skor.ToString();
            teksHasilSkor.gameObject.SetActive(true);
            teksLiveScore.gameObject.SetActive(false);

            if (isGameFinished)
            {
                GiveGold();
            }
        }
    }

    private void SaveScore()
    {
        PlayerPrefs.SetInt("Skor", skor);
        PlayerPrefs.Save();
    }

    private void SaveGold()
    {
        PlayerPrefs.SetInt("Gold3", gold);
        PlayerPrefs.Save();
    }

    private void UpdateSoal()
    {
        if (nilaiAcak >= KumpulanSoal.Count)
        {
            Debug.LogError("Invalid random index: " + nilaiAcak);
            return;
        }

        Soal soal = KumpulanSoal[nilaiAcak];
        textSoal.text = soal.soal;
        gambarSoal.sprite = soal.gambarSoal;

        imageA.sprite = soal.pilA;
        imageB.sprite = soal.pilB;
        imageC.sprite = soal.pilC;
        imageD.sprite = soal.pilD;

        ResetAnswerFlags();
        ResetTimer();
        isTimerRunning = true; // Memulai timer untuk pertanyaan saat ini
    }

    private void NextQuestion()
    {
        KumpulanSoal.RemoveAt(nilaiAcak);
        if (KumpulanSoal.Count > 0)
        {
            nilaiAcak = 0; // Mulai dengan indeks pertama
            UpdateSoal();
            isTimerRunning = true; // Memulai timer untuk pertanyaan berikutnya
        }
        else
        {
            teksSkor.text = "Skor : " + skor.ToString();

            if (skor < 7)
            {
                ShowGameOverPanel();
                GiveGold();
            }
            else if (skor >= 8)
            {
                ShowWinPanel();
                GiveGold();
                UnlockNewLevel();
            }

            isGameFinished = true;
            Debug.Log("All questions answered. Time stopped.");

            teksHasilSkor.text = "Your Score: " + skor.ToString();
            teksHasilSkor.gameObject.SetActive(true);
            teksLiveScore.gameObject.SetActive(false);

            if (isGameFinished)
            {
                GiveGold();
            }
        }
    }

    private void ResetAnswerFlags()
    {
        isAnswerGiven = false;
        answerIndex1 = -1;
        answerIndex2 = -1;
    }

    private void ResetTimer()
    {
        waktuPerSoal = initialTime; // Mereset timer setiap kali pertanyaan berikutnya muncul
        jawabanTimeLeft = jawabanTimer; // Mereset waktu jawaban setiap kali pertanyaan berikutnya muncul
    }

    private void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);
        SaveScore();
        SaveGold();
        winPanel.SetActive(true);
        
    }

    private void ShowWinPanel()
    {
        SaveScore();
        SaveGold();
        winPanel.SetActive(true);
        UnlockNewLevel();
    }

    private void ReduceLifeChance()
    {
        if (!isAnswerGiven)
        {
            lifeChances--;
            Debug.Log("Skipped question. Remaining life chances: " + lifeChances);

            if (lifeChances < 1)
            {
                isGameFinished = true;
                Debug.Log("No more life chances. Game over.");
                ShowGameOverPanel();
                teksHasilSkor.text = "Your Score: " + skor.ToString();
                teksHasilSkor.gameObject.SetActive(true);
                teksLiveScore.gameObject.SetActive(false);
                return;
            }

            CheckGameOver();
            UpdateLifeChanceSprites();
        }
    }

    private void CheckGameOver()
    {
        if (lifeChances < 1)
        {
            isGameFinished = true;
            Debug.Log("No more life chances. Game over.");
            ShowGameOverPanel();
            teksHasilSkor.text = "Your Score: " + skor.ToString();
            teksHasilSkor.gameObject.SetActive(true);
            teksLiveScore.gameObject.SetActive(false);
        }
    }

    private void SetLifeChanceSprites()
    {
        for (int i = 0; i < lifeChanceImages.Length; i++)
        {
            lifeChanceImages[i].sprite = lifeChanceFullSprite;
        }
    }

    private void UpdateLifeChanceSprites()
    {
        if (lifeChances >= 0 && lifeChances < lifeChanceImages.Length)
        {
            lifeChanceImages[lifeChances].sprite = lifeChanceEmptySprite;
        }
    }

    private void GiveGold()
    {
        int totalGold =  goldAmount;
        teksGold.text = "Gold : " + totalGold.ToString(); // Menampilkan jumlah poin emas di game
        // Lakukan aksi berdasarkan poin emas yang didapatkan, seperti menambahkan ke saldo pemain atau menampilkan dalam UI
        // Misalnya: playerAccount.AddGold(totalGold);
        // Atau: goldText.text = "Gold : " + totalGold.ToString()
    }

    private void ShuffleSoal()
    {
        // Mengacak urutan soal menggunakan algoritma Fisher-Yates
        for (int i = KumpulanSoal.Count - 1; i > 0; i--)
        {
            int j = Random.Range(0, i + 1);
            Soal temp = KumpulanSoal[i];
            KumpulanSoal[i] = KumpulanSoal[j];
            KumpulanSoal[j] = temp;
        }
    }

    private bool GetAnswerByIndex(Soal soal, int index)
    {
        switch (index)
        {
            case 0:
                return soal.A;
            case 1:
                return soal.B;
            case 2:
                return soal.C;
            case 3:
                return soal.D;
            default:
                return false;
        }
    }

    void UnlockNewLevel()
    {
        //  if (SceneManager.GetActiveScene().buildIndex >= PlayerPrefs.GetInt("ReachedIndex"))
        // {
        //     PlayerPrefs.SetInt("ReachedIndex", SceneManager.GetActiveScene().buildIndex + 1);
        //     PlayerPrefs.SetInt("Skor", PlayerPrefs.GetInt("Skor", 1) + 1);
        //     PlayerPrefs.Save();
        // }
        PlayerPrefs.SetInt("UnlockedLevel", PlayerPrefs.GetInt("UnlockedLevel", 1) + 1);
        PlayerPrefs.Save();
    }

    // private void PlaySound(AudioClip sound)
    // {
    //     audioSource.PlayOneShot(sound);
    // }
}
