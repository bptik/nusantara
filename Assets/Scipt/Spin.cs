using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyUI.PickerWheelUI;

public class Spin : MonoBehaviour
{
    [SerializeField] public Button uiSpinButton;
    [SerializeField] public Text uiSpinButtonText;

    [SerializeField] public PickerWheel pickerWheel;
    
    // Start is called before the first frame update
    void Start()
    {
        uiSpinButton.onClick.AddListener(() => {
            uiSpinButtonText.text = "Spinning";
            pickerWheel.Spin();
        });
    } 
}
