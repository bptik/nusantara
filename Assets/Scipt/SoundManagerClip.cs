using UnityEngine;

public class SoundManagerClip : MonoBehaviour
{
    public static SoundManagerClip Instance { get; private set; }

    private AudioSource soundSource;

    private void Awake()
    {
        // Pastikan hanya ada satu instance AudioManager
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        // Inisialisasi AudioSources atau konfigurasi suara lainnya
        soundSource = gameObject.AddComponent<AudioSource>();
    }

    // Fungsi untuk memutar suara
    public void PlaySound(AudioClip clip)
    {
        soundSource.clip = clip;
        soundSource.Play();
    }

    public void PauseSound(AudioClip clip)
    {
        soundSource.clip = clip;
        soundSource.Stop();
    }

    // Fungsi lain untuk mengontrol suara sesuai kebutuhan
}
