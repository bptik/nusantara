using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoalManagerlvl2 : MonoBehaviour
{
    [System.Serializable]
    public class Soal
    {
        [TextArea]
        [Header("Soal")]
        public string soal;

        [Header("Pilihan untuk jawaban")]
        public string pilA;
        public string pilB, pilC, pilD;

        [Header("Kunci Jawaban")]
        public bool A;
        public bool B, C, D;
    }

    public float waktuPerSoal;
    public float jawabanTimer; // Timer untuk setiap jawaban
    private int nilaiAcak;

    public Text textSoal;
    public Text textA, textB, textC, textD;

    public Text textWaktu;
    public Text teksSkor;
    public Text teksHasilSkor;
    public Text teksLiveScore;
    public Text teksGold; // Teks untuk menampilkan poin emas
    public Image[] lifeChanceImages;
    public Sprite lifeChanceFullSprite;
    public Sprite lifeChanceEmptySprite;

    public GameObject gameOverPanel;
    public GameObject winPanel;

    public List<Soal> KumpulanSoal;
    private int skor = 0;
    private int lifeChances = 3;
    private int gold = 0;

    private bool isGameFinished = false;
    private bool isAnswerGiven = false;
    private int answerIndex = -1;

    private float initialTime;
    private float jawabanTimeLeft; // Waktu yang tersisa untuk menjawab

    public AudioClip soundJawabanBenar;
    public AudioClip soundJawabanSalah;
    public AudioClip soundTimeOut;
    private AudioSource audioSource;

    public int goldAmount = 10000;

    private bool isTimerRunning = false;
    

    void Start()
    {
        teksLiveScore.text = "Score : 0";
        SetLifeChanceSprites();
        initialTime = waktuPerSoal;
        jawabanTimeLeft = jawabanTimer; // Set waktu jawaban awal
        int skor  = PlayerPrefs.GetInt("Skor", 0);
        int goldAmount = PlayerPrefs.GetInt("Gold2", 0);
        audioSource = GetComponent<AudioSource>();

        
        teksGold.text = "Gold : " + gold.ToString();

        ShuffleSoal();
        nilaiAcak = 0;

        UpdateSoal();
    }

    void Update()
    {
        if (isGameFinished)
        {
            return;
        }

        textWaktu.text = "Time : " + waktuPerSoal.ToString("0.0");

        if (isTimerRunning)
        {
            waktuPerSoal -= Time.deltaTime;

            if (waktuPerSoal <= 0)
            {
                if (!isAnswerGiven)
                {
                    ReduceLifeChance();
                }

                PlaySound(soundTimeOut);
                ResetAnswerFlags();
                isTimerRunning = false;

                NextQuestion();
            }
        }

        if (isAnswerGiven)
        {
            jawabanTimeLeft -= Time.deltaTime;

            if (jawabanTimeLeft <= 0)
            {
                ReduceLifeChance();
                PlaySound(soundTimeOut);
                ResetAnswerFlags();
                isTimerRunning = false;
            }
        }
    }

    public void CekJawaban(int index)
    {
        if (isGameFinished)
        {
            return;
        }

        if (!isAnswerGiven)
        {
            answerIndex = index;
            isAnswerGiven = true;
            isTimerRunning = false;
        }

        if (isAnswerGiven)
        {
            bool kunciJawaban = GetAnswerByIndex(KumpulanSoal[nilaiAcak], answerIndex);

            if (kunciJawaban)
            {
                skor++;
                audioSource.PlayOneShot(soundJawabanBenar);
                GiveGold();
            }
            else
            {
                ReduceLifeChance();
                audioSource.PlayOneShot(soundJawabanSalah);
                UpdateLifeChanceSprites();
            }

            ResetAnswerFlags();

            if (!kunciJawaban)
            {
                ReduceLifeChance();

                if (lifeChances < 1)
                {
                    isGameFinished = true;
                    Debug.Log("No more life chances. Game over.");
                    ShowGameOverPanel();
                    teksHasilSkor.gameObject.SetActive(true);
                    teksLiveScore.gameObject.SetActive(false);
                    return;
                }
                else
                {
                    Debug.Log("Incorrect answer. Remaining life chances: " + lifeChances);
                    UpdateLifeChanceSprites();
                }
            }

            KumpulanSoal.RemoveAt(nilaiAcak);
            if (KumpulanSoal.Count > 0)
            {
                nilaiAcak = 0;
                UpdateSoal();
                isTimerRunning = true;
            }
            else
            {
                teksSkor.text = "Skor : " + skor.ToString();

                if (skor < 7)
                {
                    ShowGameOverPanel();
                    GiveGold();
                }
                else if (skor >= 8)
                {
                    ShowWinPanel();
                    GiveGold();
                    UnlockNewLevel();
                }

                isGameFinished = true;
                Debug.Log("All questions answered. Time stopped.");

                teksHasilSkor.text = "Your Score: " + skor.ToString();
                teksHasilSkor.gameObject.SetActive(true);
                teksLiveScore.gameObject.SetActive(false);

                if (isGameFinished)
                {
                    GiveGold();
                }
            }
        }

        ResetTimer();
        teksLiveScore.text = "Skor : " + skor.ToString();
    }

    public void SkipJawaban()
    {
        if (isGameFinished || isAnswerGiven)
        {
            return;
        }

        if (!isAnswerGiven)
        {
            ReduceLifeChance();
            UpdateLifeChanceSprites();
            ResetTimer();
            isTimerRunning = true;
        }

        KumpulanSoal.RemoveAt(nilaiAcak);
        waktuPerSoal = initialTime;

        if (KumpulanSoal.Count > 0)
        {
            nilaiAcak = 0;
            UpdateSoal();
        }
        else
        {
            teksSkor.text = "Skor : " + skor.ToString();

            if (skor < 7)
            {
                ShowGameOverPanel();
            }
            else if (skor >= 8)
            {
                ShowWinPanel();
                GiveGold();
                UnlockNewLevel();
            }

            isGameFinished = true;
            Debug.Log("All questions answered. Time stopped.");

            teksHasilSkor.text = "Your Score: " + skor.ToString();
            teksHasilSkor.gameObject.SetActive(true);
            teksLiveScore.gameObject.SetActive(false);

            if (isGameFinished)
            {
                GiveGold();
            }
        }
    }


    private void SaveScore()
    {
        PlayerPrefs.SetInt("Skor", skor);
        PlayerPrefs.Save();
    }

    private void SaveGold()
    {
        PlayerPrefs.SetInt("Gold2", gold);
        PlayerPrefs.Save();
    }
    

    private void UpdateSoal()
    {
        if (nilaiAcak >= KumpulanSoal.Count)
        {
            Debug.LogError("Invalid random index: " + nilaiAcak);
            return;
        }

        Soal soal = KumpulanSoal[nilaiAcak];
        textSoal.text = soal.soal;

        textA.text = soal.pilA;
        textB.text = soal.pilB;
        textC.text = soal.pilC;
        textD.text = soal.pilD;

        ResetAnswerFlags();
        ResetTimer();
        isTimerRunning = true;
    }

    private void NextQuestion()
    {
        KumpulanSoal.RemoveAt(nilaiAcak);
        if (KumpulanSoal.Count > 0)
        {
            nilaiAcak = 0;
            UpdateSoal();
            isTimerRunning = true;
        }
        else
        {
            teksSkor.text = "Skor : " + skor.ToString();

            if (skor < 7)
            {
                ShowGameOverPanel();
                GiveGold();
            }
            else if (skor >= 8)
            {
                ShowWinPanel();
                GiveGold();
                UnlockNewLevel();
            }

            isGameFinished = true;
            Debug.Log("All questions answered. Time stopped.");

            teksHasilSkor.text = "Your Score: " + skor.ToString();
            teksHasilSkor.gameObject.SetActive(true);
            teksLiveScore.gameObject.SetActive(false);

            if (isGameFinished)
            {
                GiveGold();
            }
        }
    }

    private void ResetAnswerFlags()
    {
        isAnswerGiven = false;
        answerIndex = -1;
    }

    private void ResetTimer()
    {
        waktuPerSoal = initialTime;
        jawabanTimeLeft = jawabanTimer;
    }

    private void ShowGameOverPanel()
    {
        SaveScore();
        SaveGold();
        gameOverPanel.SetActive(true);
    }

    private void ShowWinPanel()
    {
        SaveScore();
        SaveGold();
        winPanel.SetActive(true);
        UnlockNewLevel();
    }

    private void ReduceLifeChance()
    {
        if (!isAnswerGiven)
        {
            lifeChances--;
            Debug.Log("Skipped question. Remaining life chances: " + lifeChances);

            if (lifeChances < 1)
            {
                isGameFinished = true;
                Debug.Log("No more life chances. Game over.");
                ShowGameOverPanel();
                teksHasilSkor.gameObject.SetActive(true);
                teksLiveScore.gameObject.SetActive(false);
                return;
            }

            CheckGameOver();
            UpdateLifeChanceSprites();
        }
    }

    private void CheckGameOver()
    {
        if (lifeChances < 1)
        {
            isGameFinished = true;
            Debug.Log("No more life chances. Game over.");
            ShowGameOverPanel();
            teksHasilSkor.gameObject.SetActive(true);
            teksLiveScore.gameObject.SetActive(false);
        }
    }

    private void SetLifeChanceSprites()
    {
        for (int i = 0; i < lifeChanceImages.Length; i++)
        {
            lifeChanceImages[i].sprite = lifeChanceFullSprite;
        }
    }

    private void UpdateLifeChanceSprites()
    {
        if (lifeChances >= 0 && lifeChances < lifeChanceImages.Length)
        {
            lifeChanceImages[lifeChances].sprite = lifeChanceEmptySprite;
        }
    }

    private void GiveGold()
    {
        int totalGold =  goldAmount;
        gold += totalGold;
        teksGold.text = "Gold : " + gold.ToString();
    }

    private void ShuffleSoal()
    {
        for (int i = KumpulanSoal.Count - 1; i > 0; i--)
        {
            int j = Random.Range(0, i + 1);
            Soal temp = KumpulanSoal[i];
            KumpulanSoal[i] = KumpulanSoal[j];
            KumpulanSoal[j] = temp;
        }
    }

    private bool GetAnswerByIndex(Soal soal, int index)
    {
        switch (index)
        {
            case 0:
                return soal.A;
            case 1:
                return soal.B;
            case 2:
                return soal.C;
            case 3:
                return soal.D;
            default:
                return false;
        }
    }

    private void PlaySound(AudioClip sound)
    {
        audioSource.PlayOneShot(sound);
    }

    void UnlockNewLevel()
    {
        //  if (SceneManager.GetActiveScene().buildIndex >= PlayerPrefs.GetInt("ReachedIndex"))
        // {
        //     PlayerPrefs.SetInt("ReachedIndex", SceneManager.GetActiveScene().buildIndex + 1);
        //     PlayerPrefs.SetInt("Skor", PlayerPrefs.GetInt("Skor", 1) + 1);
        //     PlayerPrefs.Save();
        // }
        PlayerPrefs.SetInt("UnlockedLevel", PlayerPrefs.GetInt("UnlockedLevel", 1) + 1);
        PlayerPrefs.Save();
    }
}
