using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public Button[] levelButtons;
    // public int minScoreToUnlock = 8;

    void Start()
    {
        int unlockLevel = PlayerPrefs.GetInt("UnlockedLevel", 1);

        // Iterasi tombol level untuk mengatur aksesibilitasnya berdasarkan skor
        for (int i = 0; i < levelButtons.Length; i++)
        {
            // bool isUnlocked = unlockLevel >= minScoreToUnlock * (i);
            levelButtons[i].interactable = false;
        }

        for (int i = 0; i < unlockLevel; i++)
        {
            //  bool isUnlocked = unlockLevel >= minScoreToUnlock * (i) + 1;
            levelButtons[i].interactable = true;
            //buttons[i].GetComponent<Image>().sprite = unlockedSprite;
        }

    }

    public void LoadLevel(int levelIndex)
    {
        SceneManager.LoadScene("PetunjukLevel" + levelIndex);
    }

    public void ResetProgress()
    {
        // Reset score menjadi 0
        PlayerPrefs.SetInt("UnlockedLevel", 1);
        PlayerPrefs.Save();

        // Reload scene agar tombol level terkunci kembali dan score menjadi 0
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
