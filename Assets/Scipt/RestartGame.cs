using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestartGame : MonoBehaviour
{
    public string gold = "Gold";
    public string skor = "Score";
    // public GameObject PanelAwal;
    // public GameObject PanelSelanjutnya;

    public void Restartlvl1()
    {
        // Reset skor dan gold ke nilai awal
        PlayerPrefs.SetInt(skor, 0);
        PlayerPrefs.SetInt(gold, 0);
        PlayerPrefs.Save();

        // Memuat ulang scene saat ini
        SceneManager.LoadScene("PetunjukLevel1");
        // PanelAwal.SetActive(false);
        // PanelSelanjutnya.SetActive(true);

     
    }

     public void Restartlvl2()
    {
        // Reset skor dan gold ke nilai awal
        PlayerPrefs.SetInt(skor, 0);
        PlayerPrefs.SetInt(gold, 0);
        PlayerPrefs.Save();

        // Memuat ulang scene saat ini
        SceneManager.LoadScene("PetunjukLevel2");
        // PanelAwal.SetActive(false);
        // PanelSelanjutnya.SetActive(true);

     
    }

    public void Restartlvl3()
    {
        // Reset skor dan gold ke nilai awal
        PlayerPrefs.SetInt(skor, 0);
        PlayerPrefs.SetInt(gold, 0);
        PlayerPrefs.Save();

        // Memuat ulang scene saat ini
        SceneManager.LoadScene("PetunjukLevel3");
        // PanelAwal.SetActive(false);
        // PanelSelanjutnya.SetActive(true);

     
    }

    public void Restartlvl4()
    {
        // Reset skor dan gold ke nilai awal
        PlayerPrefs.SetInt(skor, 0);
        PlayerPrefs.SetInt(gold, 0);
        PlayerPrefs.Save();

        // Memuat ulang scene saat ini
        SceneManager.LoadScene("PetunjukLevel4");
        // PanelAwal.SetActive(false);
        // PanelSelanjutnya.SetActive(true);

     
    }

    public void RestartWhell()
    {
        // Reset skor dan gold ke nilai awal
        PlayerPrefs.SetInt(skor, 0);
        PlayerPrefs.SetInt(gold, 0);
        PlayerPrefs.Save();

        // Memuat ulang scene saat ini
        SceneManager.LoadScene("PetunjukLevel4");
        // PanelAwal.SetActive(false);
        // PanelSelanjutnya.SetActive(true);

    }


}
