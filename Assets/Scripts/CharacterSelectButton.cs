using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class CharacterSelectButton : MonoBehaviour
    {
        [SerializeField] CharacterData characterData;
        [SerializeField] Image backgroundImage;
        [SerializeField] TextMeshProUGUI characterName;
        [SerializeField] Image mainImage;

		[SerializeField] Color activeColor;
		[SerializeField] Color inactiveColor;

		public MissionSelectionManager manager;

		private void Start()
        {
            mainImage.sprite = characterData.characterPortrait;
            characterName.text = characterData.characterName;
        }

        public void SetButton(CharacterData data)
            => characterData = data;

        public void SetColor(bool selected)
        {
            if (selected)
                backgroundImage.color = mainImage.color = activeColor;
            else
                backgroundImage.color = mainImage.color = inactiveColor;
        }

        public void SelectButton()
        {
			manager.CheckSelectedCharacter(this);
			Global.GlobalData.SelectedCharacterData = characterData;
        }
    }
}
