using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Herghys
{
    public sealed class CursorManager : MonoBehaviour
    {
        [SerializeField] Texture2D baseCursor, crosshairCursor;
        [SerializeField] bool dontDestroyOnLoad = false;
        [SerializeField] bool crossHairOnStart = false;

        private Vector2 hotspot;
        public static CursorManager Instance { get; private set; }
        private void Awake()
        {
            Instance = this;
            if (dontDestroyOnLoad)
            {
                if (Instance != null) Destroy(Instance.gameObject);
                DontDestroyOnLoad(this);
            }
        }

        private void Start()
        {
            if (crossHairOnStart) SetCursor(false);
        }

        public void Revert()
        {
			hotspot = new Vector2(baseCursor.width / 2, baseCursor.height / 2);
			Cursor.SetCursor(baseCursor, hotspot, CursorMode.Auto);
		}

        public void SetCursor(bool useBase)
        {
            if (useBase) SetCursor(baseCursor);
            else SetCursor(crosshairCursor);
        }

        public void SetCursor(Texture2D cursor)
        {
            hotspot = new Vector2(cursor.width/2, cursor.height/2);
            Cursor.SetCursor(cursor, hotspot, CursorMode.Auto);
        }
    }
}
