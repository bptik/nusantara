using System;
using System.Collections.Generic;
using DG.Tweening;
using Herghys.Global;
using KoganeUnityLib;
using Redcode.Awaiting;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class MissionSelectionManager : MonoBehaviour
    {
        //[SerializeField] PopUp popUp;

        [SerializeField] SceneFader fader;
        [SerializeField] TextMeshProUGUI missionPreviewTMP;
        [SerializeField] int missionIndex;
        [SerializeField] SpriteRenderer backgroundRenderer;
        [SerializeField] bool dataSelected;

        [Header("Guide")]
        [TextArea(1, 100)]
        [SerializeField] string guideText;
        [SerializeField] float textSpeed = 10;
        [SerializeField] TMP_Typewriter guideTMP;
        [SerializeField] Canvas guideCanvas;
        [SerializeField] Canvas mainCanvas;
        [SerializeField] Sprite guideBackground;
        [SerializeField] Sprite temporaryBackgroumd;
        [SerializeField] AudioClip guideClip;
        [SerializeField] GameObject character;

        [Header("Audio")]
        [SerializeField] AudioSource audioSource;

        [Header("Level")]
        [SerializeField] string selectedLevelName = string.Empty;
        [SerializeField] bool selectedMissionStarReq;

        [SerializeField] int starCount;

        [Header("Prefabs")]
        [SerializeField] Transform characterButtonParent;
        [SerializeField] CharacterSelectButton characterButtonPrefab;
        [SerializeField] Transform weaponButtonParent;
        [SerializeField] WeaponSelectButton weaponButtonPrefab;

        [Header("Selected")]
        [SerializeField] GameObject target;
        [SerializeField] MissionDataContent selectedData;
        [SerializeField] WeaponData selectedWeaponData;
        [SerializeField] CharacterData selectedCharacterData;

        [SerializeField] List<CharacterSelectButton> spawnedCharacterButton;
        [SerializeField] List<WeaponSelectButton> spawnedWeaponButton;

        [Header("Data")]
        [SerializeField] List<MissionData> gameDatas;
        [SerializeField] List<Character> characters;
        [SerializeField] List<Weapon> weapons;

        [Header("Misc")]
        [SerializeField] GameObject characterSelectPanel;
        [SerializeField] bool canOpenCharacterSelection;
        [SerializeField] GameObject weaponSelectPanel;
        [SerializeField] bool canOpenWeaponSelection;
        bool targetActive, characterActive, bgActive;

        [SerializeField] Button firstNextButton;
        bool guideSKipped;

        private void Awake()
        {
            fader.FadeOut();
            if (firstNextButton != null) firstNextButton.interactable = false;
            targetActive = characterActive = false;
        }

        private void OnEnable()
        {
            GlobalEvents.MissionSelected += OnMissionSelected;
            //popUp.startButton.onClick.AddListener(StartGame);
        }

        private void OnDisable()
        {
            GlobalEvents.MissionSelected -= OnMissionSelected;
            //popUp.startButton.onClick.RemoveListener(StartGame);
        }

        private void Start()
        {
            SetInitialData();
            SpawnCharacterButton();
            SpawnWeaponButton();
            ToggleCharacter(characterActive);
            ToggleTarget(targetActive);
        }

        void SetInitialData()
        {
            starCount = PlayerPrefs.GetInt(GlobalData.StarCount);
            temporaryBackgroumd = guideBackground;
        }

        void SpawnCharacterButton()
        {
            characters.ForEach(character =>
            {
                CharacterSelectButton characterButton = Instantiate(characterButtonPrefab, characterButtonParent);
                characterButton.SetButton(character.data);
                characterButton.manager = this;
                spawnedCharacterButton.Add(characterButton);
            });
            spawnedCharacterButton[0].SelectButton();
        }

        void SpawnWeaponButton()
        {
            weapons.ForEach(weapon =>
            {
                WeaponSelectButton weaponButton = Instantiate(weaponButtonPrefab, weaponButtonParent);
                weaponButton.SetButton(weapon.weaponData);
                weaponButton.manager = this;
                spawnedWeaponButton.Add(weaponButton);
            });
            spawnedWeaponButton[0].SelectButton();
        }

        public void BackToMenu()
            => BackToMenuAsync();

        async void BackToMenuAsync()
        {
            var op = SceneManager.LoadSceneAsync("MainMenu");
            op.allowSceneActivation = false;
            await fader.FadeInTask();
            DOTween.Clear(true);
            await new WaitForSeconds(0.05f);
            op.allowSceneActivation = true;
        }

        public void SetLevelToLoad(int missionNumber, string levelName, bool starReq)
        {
            selectedLevelName = levelName;
            selectedMissionStarReq = starReq;
        }

        public void CheckSelectedCharacter(CharacterSelectButton button)
        {
            spawnedCharacterButton.ForEach(btn =>
            {
                if (btn != button) btn.SetColor(false);
                else btn.SetColor(true);
            });
        }

        public void CheckSelectedWeapon(WeaponSelectButton button)
        {
            spawnedWeaponButton.ForEach(btn =>
            {
                if (btn != button) btn.SetColor(false);
                else btn.SetColor(true);
            });
        }


        private void OnMissionSelected(int missionDataIndex)
        {
            if (!firstNextButton.interactable)
                firstNextButton.interactable = true;

            audioSource.Stop();
            dataSelected = true;
            missionIndex = missionDataIndex;
            selectedData = gameDatas[missionIndex].gameData;
            GlobalData.SelectedMissionData = selectedData;
            //GlobalData.MissionNumber = mis

            characterActive = true;
            targetActive = true;

            ToggleTarget(true);
            ToggleCharacter(true);
            //target.SetActive(true);
            //popUp.SetUpText(selectedData.previewPopUpText);

            audioSource.clip = selectedData.guideClip;
            audioSource.Play();
            missionPreviewTMP.text = selectedData.previewText;
            missionPreviewTMP.alignment = TextAlignmentOptions.Left;
            temporaryBackgroumd = selectedData.previewImage;
            backgroundRenderer.sprite = selectedData.previewImage;
        }

        public void ToggleTarget(bool active)
        {
            target.SetActive(active);
        }

        public void OpenGuide()
        {
            StartGuide();
        }

        void StartGuide()
        {
            backgroundRenderer.sprite = guideBackground;
            ToggleCharacter(false);
            ToggleTarget(false);
            guideCanvas.enabled = true;
            mainCanvas.enabled = false;
            if (guideClip)
            {
                audioSource.clip = guideClip;
                audioSource.Play();
            }
            guideTMP.Play(guideText, textSpeed, OnGuideComplete);

        }

        public void SkipGuide()
        {
            guideSKipped = true;
            guideTMP.Skip(true);
        }



        private void OnGuideComplete()
        {
            if (!guideSKipped)
                WaitAudio();
            else
                StopGuide();
        }

        private async void WaitAudio()
        {
            while (audioSource.isPlaying)
            {
                await new WaitForSeconds(0);
            }
            StopGuide();

            //character.SetActive(true);
            /*if (dataSelected)
            {
                ToggleCharacter(true);
                ToggleTarget(true);
            }*/
        }

        private void StopGuide()
        {
			audioSource.Stop();
			guideCanvas.enabled = false;
			mainCanvas.enabled = true;
			backgroundRenderer.sprite = temporaryBackgroumd;
            guideSKipped = false;

			ToggleCharacter(characterActive);
			ToggleTarget(targetActive);
		}

        public void RefreshCanvas()
        {
            Canvas.ForceUpdateCanvases();
        }

        public void ToggleCharacter(bool active)
        {
            character.SetActive(active);
        }

        public void OpenNextSelection(GameObject panel)
        {
            //target.SetActive(false);
            if (selectedData != null && !string.IsNullOrEmpty(selectedLevelName))
            {
                audioSource.Stop();
                ToggleTarget(false);
                ToggleCharacter(false);

                if (GlobalData.CharacterSelected)
                    StartGame();
                else
                    panel.SetActive(true);
            }
        }

        public void OpenCharacterSelection()
        {
            if (selectedData == null && string.IsNullOrEmpty(selectedLevelName))
                return;

			audioSource.Stop();
			ToggleTarget(false);
			ToggleCharacter(false);
			characterSelectPanel.SetActive(true);

			/*if (canOpenCharacterSelection)
            {
                
            }
            else
            {
                if (GlobalData.CharacterSelected)
                    StartGame();
            }*/
        }

        public void OpenWeaponSelection()
        {
            if (canOpenWeaponSelection)
            {
                characterSelectPanel.SetActive(false);
                weaponSelectPanel.SetActive(true) ;
            }
            else
            { 
					StartGame();
			}
        }


        public void SetSelection(bool characterSelect, bool weaponSelection)
        {
            canOpenCharacterSelection = characterSelect;
            canOpenWeaponSelection = weaponSelection;
        }

        public void HideCurrentSelection(GameObject panel)
        {
			if (selectedData != null && !string.IsNullOrEmpty(selectedLevelName))
			    panel.SetActive(false);
        }

        public void Next()
        {
            if (selectedData != null && !string.IsNullOrEmpty(selectedLevelName))
            {
                audioSource.Stop();
                StartGame();
            }
            //popUp.gameObject.SetActive(true);
        }

        public void StartGame()
        {
            if (selectedMissionStarReq)
            {
                if (starCount <= 0) return;
                starCount -= 1;
                PlayerPrefs.SetInt(GlobalData.StarCount, starCount);
            }

            //SceneManager.LoadScene(selectedLevelName);
            LoadScene();

            //Debug.Log(Global.GlobalData.LevelName);
            //SceneManager.LoadScene(Global.GlobalData.LevelName);
        }

        async void LoadScene()
        {
            var op = SceneManager.LoadSceneAsync(selectedLevelName);
            GlobalData.CharacterSelected = true;
            op.allowSceneActivation = false;
			await fader.FadeInTask();
			DOTween.Clear(true);
            await new WaitForSeconds(0.05f);
            op.allowSceneActivation = true;
		}
    }
}
