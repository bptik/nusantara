using System;
using System.Collections;
using System.Collections.Generic;
using Herghys.Global;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Herghys
{
    public class SplashManager : MonoBehaviour
    {
        private void Awake()
        {
			if (string.IsNullOrEmpty(PlayerPrefs.GetString(GlobalData.LastAccessed)))
				GlobalData.SetInitialValue();

			PlayerPrefs.SetString(GlobalData.LastAccessed, DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
		}
        void Start()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
