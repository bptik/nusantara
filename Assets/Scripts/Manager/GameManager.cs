using System;
using SystemRandom = System.Random;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Herghys.Global;
using Redcode.Awaiting;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

namespace Herghys
{
    public sealed class GameManager : MonoBehaviour
    {
        [SerializeField] int sceneIndex;
        [SerializeField] string sceneName;

        [Header("Reference")]
        [SerializeField] TextMeshProUGUI tantanganText;
        [SerializeField] Scoring scoring;
        [SerializeField] SceneFader fader;
        [SerializeField] int missionIndex = 0;
        [SerializeField] int missionNumber = 0;
		[SerializeField] QuestionData questionData;
		[SerializeField] List<MissionData> missionData;
        [SerializeField] MissionDataContent gameData;
        [SerializeField] PlayerController playerController;
        [HideInInspector] public CharacterData characterData;
		[HideInInspector] public WeaponData weaponData;
        //[SerializeField] Image challengeBox;

        [Header("Game State")]
        public GameState gameState;

        [Header("UI")]
        public Canvas GameCavnas;
        [SerializeField] PopUp popUp;
        [SerializeField] EndPopUp winPopUp;
        [SerializeField] EndPopUp losePopUp;
        public GameObject background;
        public GameObject finishScreen;
        public GameObject startScreen;

        [Header("Audio")]
        public AudioClip errorAudio;
        public AudioClip correctAudio;
        public AudioClip wrongAudio;
        public AudioSource audioSource;


        [Header("HInts")]
        [SerializeField] Hints hint;
        [SerializeField] int totalSticks;
        public int TotalSticks { get => totalSticks; }

        [Header("Scoring")]
        public int totalScore;
        [SerializeField] int minimumScore;
		[SerializeField] int correctAnswerScore;

        [Header("Timer")]
        [SerializeField] Timer timer;
        public float timerTime;

        [Header("After Finish")]
        [SerializeField] int totalStar;
        [SerializeField] int receviedStar;
        [SerializeField] int receviedStick;

        [Header("Questions")]
        [SerializeField] int totalQuestion;
		[SerializeField] int questionIndex = 0;
		[SerializeField] TMP_SpriteAsset spriteAsset;

        [Header("Answers")]
        public AnswerData answerPrefab;
        [SerializeField] List<Sprite> spriteTantangan;
        [SerializeField] List<AnswerArea> answerSpawnPoint;
        [SerializeField] List<AnswerData> answerDatas;
        [SerializeField] List<Answer> answers;

        SystemRandom sysRand;
        

        private void Awake()
        {
            fader.FadeOut();
            SetGameData();

            sceneIndex = SceneManager.GetActiveScene().buildIndex;
            sceneName = SceneManager.GetActiveScene().name;

            InitializeGameData();
            if (!playerController) playerController = FindObjectOfType<PlayerController>();
            characterData = GlobalData.SelectedCharacterData;
            weaponData = GlobalData.SelectedWeaponData;
        }

        private void Start()
        {
            if (totalSticks < 1) hint.gameObject.SetActive(false);
            else hint.gameObject.SetActive(true);
            popUp.SetUpText(gameData.previewPopUpText);
            if (characterData !=null && characterData.bodySprite != null)
                playerController.SetCharacter(characterData);

            if (weaponData != null && weaponData.weaponAim != null)
            {
                playerController.SetCharacter(weaponData);
                answerSpawnPoint.ForEach(area =>
                {
                    area.answerController.SetArrowSprite(weaponData.projectileCut);
                });
            }			
        }

        void SetGameData()
        {
            if (GlobalData.SelectedMissionData != null)
            {
                gameData = GlobalData.SelectedMissionData;
                
            }
            else
                gameData = missionData[missionIndex].gameData;

			missionNumber = gameData.level;
		}

        void InitializeGameData()
        {
            receviedStar = gameData.star;
            receviedStick = gameData.stick;

			spriteAsset = gameData.spriteAsset;
            minimumScore = gameData.minimumScore;
            correctAnswerScore = gameData.correctAnswerScore;
            totalQuestion = gameData.questionsData.Count;
            //totalQuestion = gameData.maxQuestion - 1;
            //totalQuestion = gameData.questionsData.Count - 1;

            timerTime = (int)TimeSpan.FromMinutes(gameData.timerTime).TotalSeconds;
            timer.SetTimer(timerTime);
            timer.manager = this;

            totalSticks = PlayerPrefs.GetInt(GlobalData.StickCount);
            totalStar = PlayerPrefs.GetInt(GlobalData.StarCount);

            winPopUp.SetStickAndStar(totalSticks, totalStar, totalScore);
            losePopUp.SetStickAndStar(totalSticks, totalStar, totalScore);

            spriteTantangan = gameData.spriteTantangan;

            scoring.SetScore(0);


            audioSource.clip = gameData.speechClip;
            audioSource.Play();
		}

		[ContextMenu("Start Game")]
		public void StartGame()
        {
            audioSource.Stop();
            GameCanvasEnabler(true);
            startScreen.SetActive(false);

            ShowQuestion();
            gameState = GameState.Play;
            timer.StartTimer();
        }

		[ContextMenu("Pause Game")]
		public void PauseGame()
        {
            gameState = GameState.Stop;
		}

		[ContextMenu("Load Next Question")]
        public async void LoadNextQuestion()
        {
            if (questionIndex < gameData.questionsData.Count - 1)
            {
                questionIndex++;
                ShowQuestion();
                await fader.FadeOutTask();
            }
        }

        [ContextMenu("Show Answers")]
        public void ShowQuestion()
        {
            //challengeBox.sprite = spriteTantangan[questionIndex];
            questionData.SetQuestionData(gameData.questionsData[questionIndex],spriteAsset);
            tantanganText.text = $"Tantangan {questionIndex+1} / {totalQuestion}";
            ShowAnswers();
        }

        public void ShowAnswers()
        {
            sysRand = new SystemRandom();
            answers = gameData.questionsData[questionIndex].answers.OrderBy(a => sysRand.Next()).ToList();
			if (answerDatas.Count <= 0)
			{
				for (int i = 0; i < answers.Count; i++)
				{
					var ans = Instantiate(answerPrefab, answerSpawnPoint[i].transform);
                    answerSpawnPoint[i].SetText(ans);
                    ans.gameManager = this;
					ans.SetData(answers[i]);
					ans.SetSpriteAsset(gameData.spriteAsset);
					answerDatas.Add(ans);
				}
				return;
			}

			for (int i = 0; i < answerDatas.Count; i++)
			{
                answerDatas[i].gameObject.SetActive(true);
				answerDatas[i].SetData(answers[i]);
			}
		}

        public async void CheckAnswers(bool isCorrect)
        {
            if (gameState != GameState.Play) return;
            if (isCorrect) CorrectAnswer();
            else WrongAnswer();

            await new WaitForSeconds(0.2f);

            if (questionIndex == totalQuestion-1)
                Finish(true);
            else
            {
                await new WaitForSeconds(0.3f);
				answerSpawnPoint.ForEach(s =>
				{
					if (s.answerController.selected == true)
						s.answerController.ResetController();
				});
				await fader.FadeInTask();
                LoadNextQuestion();
            }
        }

        public void BackToMenu()
        {
            audioSource.Stop();
            CursorManager.Instance.Revert();
            BackToMenuAsync();
        }

        async void BackToMenuAsync()
        {
			var op = SceneManager.LoadSceneAsync("MissionSelection");
			op.allowSceneActivation = false;
			await fader.FadeInTask();
			DOTween.Clear(true);
			await new WaitForSeconds(0.05f);
			op.allowSceneActivation = true;
		}

        public void RestartScene()
        {
            audioSource.Stop();
            Restart();
        }

        async void Restart()
        {
			if (totalStar >= 1)
			{
                //if (missionNumber <= 1) return;
                await fader.FadeInTask();
                if (missionNumber > 1)
                {
                    totalStar--;
                    losePopUp.SetStickAndStar(totalSticks, totalStar, totalScore);
                    PlayerPrefs.SetInt(GlobalData.StarCount, totalStar);
                }

				await new WaitForSeconds(0.25f);
				DOTween.Clear(true);
				SceneManager.LoadScene(sceneIndex);
			}
			else
			{
				//Play Error Sound
			}
		}

        [ContextMenu("Show Hints")]
        public void ShowAnswerHints()
        {
            totalSticks--;
			answerDatas
                .Select(x => x)
                .Where(x => !x.isCorrect && x.gameObject.activeSelf)
                .Take(2).ToList().ForEach(x => x.gameObject.SetActive(false));
        }

        void CorrectAnswer()
        {
            totalScore += correctAnswerScore;
			scoring.SetScore(totalScore);
			if (correctAudio) audioSource.PlayOneShot(correctAudio);
        }

        void WrongAnswer()
        {
			if (wrongAudio) audioSource.PlayOneShot(wrongAudio);
		}

        async void WinGame()
        {
            audioSource.clip = gameData.winClip;
            audioSource.Play();

            winPopUp.SetUpText(gameData.winPopUpText);
			winPopUp.gameObject.SetActive(true);
            totalStar += receviedStar;
            totalSticks += receviedStick;

            await new WaitForSeconds(0.25f);
			winPopUp.SetStickAndStar(totalSticks, totalStar, totalScore);

            var unlockedLevel = PlayerPrefs.GetInt(GlobalData.UnlockedLevelID);
            if(unlockedLevel == missionNumber)
            {
                var newLevel = missionNumber + 1;
                PlayerPrefs.SetInt(GlobalData.UnlockedLevelID, newLevel);
            }


			PlayerPrefs.SetInt(GlobalData.StarCount, totalStar);
            PlayerPrefs.SetInt(GlobalData.StickCount, totalSticks);
            if (gameData.shouldUnlockWeapon)
                PlayerPrefs.SetString(GlobalData.WeaponUnlocked, bool.TrueString);
		}

        void LoseGame()
        {
			audioSource.clip = gameData.winClip;
			audioSource.Stop();

			if (missionNumber >1)
                PlayerPrefs.SetString(GlobalData.StarRequirementID(missionNumber), bool.TrueString);

			losePopUp.SetStickAndStar(totalSticks, totalStar, totalScore);
			losePopUp.SetUpText(gameData.losePopUpText);
            losePopUp.gameObject.SetActive(true);
		}

        public void Finish(bool shouldStopTimer)
        {
			gameState = GameState.Stop;
            if (shouldStopTimer)
                timer.StopTimer();
            EndGame();
		}

        void GameCanvasEnabler(bool enable)
        {
			GameCavnas.enabled = enable;
            background.SetActive(!enable);
		}

        void EndGame()
        {
            //Disable Timer
            gameState = GameState.Stop;
            GameCanvasEnabler(false);
            finishScreen.SetActive(true);

            scoring.gameObject.SetActive(false);

			Debug.Log($"Total Score: {totalScore}");
            if (totalScore >= minimumScore) WinGame();
            else LoseGame();
        }
    }
}
