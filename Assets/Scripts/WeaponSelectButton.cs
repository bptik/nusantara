using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class WeaponSelectButton : MonoBehaviour
    {
		[SerializeField] bool isDisabled;
		[SerializeField] int index;
		[SerializeField] Button button;

		[SerializeField] WeaponData weaponData;
		[SerializeField] TextMeshProUGUI weaponName;
		[SerializeField] Image backgroundImage;
		[SerializeField] Image mainImage;

		[SerializeField] Color activeColor;
		[SerializeField] Color inactiveColor;
		[SerializeField] Color disabledColor;

		public MissionSelectionManager manager;

		private void Awake()
		{
			if (!button) button = GetComponent<Button>();
		}

		public void Start()
		{
			mainImage.sprite = weaponData.weaponPortrait;
			if (!bool.Parse(PlayerPrefs.GetString(Global.GlobalData.WeaponUnlocked)))
			{
				if (index > 0)
				{
					button.interactable = false;
					backgroundImage.color = disabledColor;
					isDisabled = true;
				}
			}
		}

		public void SetButton(WeaponData data)
		{
			weaponName.text = data.weaponName;
			weaponData = data;
			index = data.weaponIndex;
		}

		public void SetColor(bool selected)
		{
			if (isDisabled) return;

			if (selected)
				backgroundImage.color = mainImage.color = activeColor;
			else
				backgroundImage.color = mainImage.color = inactiveColor;
		}

		public void SelectButton()
		{
			manager.CheckSelectedWeapon(this);
			Global.GlobalData.SelectedWeaponData = weaponData;
		}
	}
}
