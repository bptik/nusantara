using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Herghys
{
    [CreateAssetMenu(fileName = "Game Data", menuName = "Data/Game Data")]
    public sealed class GameData : ScriptableObject
    {
        [Header("Level Preview")]
        public Sprite previewImage;
        public AudioClip previewClip;
        [TextArea(1,100)]
        public string previewText;
        [TextArea(1, 100)]
        public string previewPopUpText;
		[TextArea(1, 100)]
		public string winPopUpText;
		[TextArea(1, 100)]
		public string losePopUpText;

		[Header("Level Data")]
        public int level;

        [Header("Scoring")]
        public int minimumScore;
        public int correctAnswerScore;

        [Header("Timer")]
        [Tooltip("Timer in minutes")]
        public float timerTime = 10;

		public TMP_SpriteAsset spriteAsset;
		public List<Question> questionsData;
    }
}
