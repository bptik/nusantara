﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Herghys
{
	[CreateAssetMenu(fileName = "Mission Data", menuName = "Data/Mission Data")]
	public class MissionData : ScriptableObject
	{
		public MissionDataContent gameData;
	}

	[Serializable]
	public class MissionDataContent
	{
		[Header("Level Preview")]
		public Sprite previewImage;
		public AudioClip previewClip;
		[TextArea(1, 100)]
		public string previewText;
		[TextArea(1, 100)]
		public string previewPopUpText;
		[TextArea(1, 100)]
		public string winPopUpText;
		[TextArea(1, 100)]
		public string losePopUpText;

		[Header("Level Data")]
		public int level;

		[Header("Received")]
		public int star;
		public int stick;

		[Header("Scoring")]
		public int minimumScore;
		public int correctAnswerScore;

		[Header("Speech")]
		public AudioClip guideClip;
		public AudioClip speechClip;
		public AudioClip winClip;
		public AudioClip loseClip;

		[Header("Timer")]
		[Tooltip("Timer in minutes")]
		public float timerTime = 10;

		public bool shouldUnlockWeapon;
		public TMP_SpriteAsset spriteAsset;
		public int maxQuestion;
		public List<Sprite> spriteTantangan;
		public List<Question> questionsData;
	}

}
