using UnityEngine;

namespace Herghys.Global
{
    public class GlobalData
    {
        public const string Level = "Level";
		public const string UnlockedLevelID = "Unclocked_Level";
		public const string StarCount = "StarCount";
        public const string StickCount = "StickCount";
        public const string LastAccessed = "LastAccessed";
        public const string WeaponUnlocked = "WeaponUnlocked";        

        public static string StarRequirementID(int level)
            => $"{Level}_{level}_StarRequired";

        public static void SetInitialValue()
        {
            PlayerPrefs.SetInt(StarCount, 1);
            PlayerPrefs.SetInt(StickCount, 0);
            PlayerPrefs.SetInt(UnlockedLevelID, 1);
            PlayerPrefs.SetString(WeaponUnlocked, bool.FalseString);
            PlayerPrefs.SetString(StarRequirementID(1), bool.FalseString);
            PlayerPrefs.SetString(StarRequirementID(2), bool.FalseString);
            PlayerPrefs.SetString(StarRequirementID(3), bool.FalseString);
            PlayerPrefs.SetString(StarRequirementID(4), bool.FalseString);
        }

		public static bool CharacterSelected { get; set; }

		public static MissionDataContent SelectedMissionData { get; set; }
        public static CharacterData SelectedCharacterData { get; set; }
        public static WeaponData SelectedWeaponData { get; set; }
    }
}

public enum AnswerType
{
    Image, Text
}

public enum GameState
{
    None, Play, Stop
}