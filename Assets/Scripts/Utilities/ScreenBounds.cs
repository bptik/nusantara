using UnityEngine;

namespace Herghys.Utilities
{
    public sealed class ScreenBounds : MonoBehaviour
    {
		[SerializeField] Camera mainCamera;

        public Vector2 BoundarySize { get; private set; }
        public float BoundaryWidth { get; private set; }
        public float BoundaryHeight { get; private set; }

        public ScreenBounds Instance;

        private void Awake()
        {
            Instance = this;
            SetBoundarySize();
        }

        void SetBoundarySize()
        {
            BoundarySize = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
            BoundaryWidth = BoundarySize.x;
            BoundaryHeight = BoundarySize.y;
		}
    }
}
