using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Herghys
{
    public sealed class GlobalEvents
    {
        public static Action<int> MissionSelected;

        public static Action<AnswerData> AnswerDataClicked;
        public static Action<AnswerData> BowReleased;
    }
}