using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using Redcode.Awaiting;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class SceneFader : MonoBehaviour
    {
        [SerializeField] Image image;
        [SerializeField] Canvas canvas;
        [SerializeField] float speed = 0.5f;

		/// <summary>
		/// Fade Out, Remove Fader
		/// </summary>
		public async void FadeOut()
        {
			Debug.Log("By Herghys || SteamID: Herghys || Github: Herghys || Discord: Herghys#4776");
			await image.DOFade(0, speed).SetEase(Ease.OutSine).AsyncWaitForCompletion();
			canvas.enabled = false;
			await new WaitForSeconds(0.15f);
		}

		/// <summary>
		/// Fade Out, Remove Fader
		/// </summary>
		public async Task FadeOutTask()
        {
			Debug.Log("By Herghys || SteamID: Herghys || Github: Herghys || Discord: Herghys#4776");
			await image.DOFade(0, speed).SetEase(Ease.OutSine).AsyncWaitForCompletion();
			canvas.enabled = false;
			await new WaitForSeconds(0.15f);
			
		}

		/// <summary>
		/// Fade In, Add the Fader
		/// </summary>
		public void FadeIn(Action OnFinish)
        {
			Debug.Log("By Herghys || SteamID: Herghys || Github: Herghys || Discord: Herghys#4776");
			canvas.enabled = true;
			image.DOFade(1, speed).SetEase(Ease.InSine);
			OnFinish();
		}

		/// <summary>
		/// Fade In, Add the Fader
		/// </summary>
		public async Task FadeInTask()
        {
			Debug.Log("By Herghys || SteamID: Herghys || Github: Herghys || Discord: Herghys#4776");
			canvas.enabled = true;
			await image.DOFade(1, speed).SetEase(Ease.InSine).AsyncWaitForCompletion();
			//DOTween.Clear();
            await new WaitForSeconds(0.15f);
		}
    }
}
