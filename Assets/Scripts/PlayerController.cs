using Redcode.Awaiting;
using UnityEngine;

namespace Herghys
{
    public sealed class PlayerController : MonoBehaviour
    {
        bool isAiming;
        Vector3 mousePosition;
        Camera mainCam;

        [Header("Character Sprites")]
        [SerializeField] SpriteRenderer head;
        [SerializeField] SpriteRenderer body;
        [SerializeField] SpriteRenderer armLeft;
        [SerializeField] SpriteRenderer armRight;

        [Header("Weapon Renderer")]
        [SerializeField] SpriteRenderer weapon;
        [SerializeField] SpriteRenderer projectile;

        public GameManager manager;
        public CharacterData characterData;
        public WeaponData weaponData;

        void Start()
        {
            if (!mainCam) Camera.main.GetComponent<Camera>();
            if (!manager) manager = FindObjectOfType<GameManager>();
        }

        private void OnEnable()
        {
            GlobalEvents.AnswerDataClicked += OnAnswerDataClicked;
        }

        private void OnDisable()
        {
            GlobalEvents.AnswerDataClicked -= OnAnswerDataClicked;
        }

        /*void LateUpdate()
        {
            if (manager.gameState == GameState.Play)
            FollowCam();
        }*/

        public void SetCharacter(WeaponData data)
        {
            if (data is null) return;
            weaponData = data;
            weapon.sprite = weaponData.weaponAim;
            projectile.sprite = weaponData.projectile;
        }

        public void SetCharacter(CharacterData data)
        {
            Debug.Log("Set Character Data");
            if (data is null ) return;
            characterData = data;
            head .sprite= characterData.headSprite;
            body.sprite = characterData.bodySprite;
            armLeft.sprite = characterData.armLeftSprite;
            armRight.sprite = characterData.armRightSprite;
        }

        void FollowCam()
        {
			mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			Vector2 direction = mousePosition - transform.position;
			float angle = Vector2.SignedAngle(Vector2.right, direction);
            if (angle > 0 && angle < 0.8) angle = 1;
            else if (angle < 0 && angle > -0.8) angle = -1;
			else if (angle > 91 && angle < 110) angle = 110;
			else if (angle < 91 && angle > 65) angle = 65;
			transform.eulerAngles = new Vector3(0, angle, 0);
		}

        async void AnimateBow(AnswerData data)
        {
            if (manager.gameState != GameState.Play) return;
            weapon.sprite = weaponData.weaponNormal;
            projectile.gameObject.SetActive(false);
			await new WaitForSeconds(0.1f);
			GlobalEvents.BowReleased?.Invoke(data);
            await new WaitForSeconds(0.5f);
            weapon.sprite = weaponData.weaponAim;
            projectile.gameObject.SetActive(true);
        }

		private void OnAnswerDataClicked(AnswerData obj)
		{
			AnimateBow(obj);
		}
	}
}
