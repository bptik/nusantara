using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace Herghys
{
    [CreateAssetMenu(fileName = "Question", menuName = "Data/Question Data")]
    public sealed class Question : ScriptableObject
    {
        [TextArea(minLines:1, maxLines:100)]
        public string questionInfo;
        public int lineSpacing = 225;
        public bool useSpriteAsset;
        public TMP_SpriteAsset spriteAsset;
        public Answer[] answers = new Answer[5];
    }
}