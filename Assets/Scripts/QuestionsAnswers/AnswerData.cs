using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class AnswerData : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI answerTextUI;
        [SerializeField] Image answerImage;


        [SerializeField] AnswerType type;
		public Button button;
        public AnswerController answerController;
        public Answer answer;
        public string answerInfo;
        public bool isCorrect;
        public GameManager gameManager;

        [SerializeField] bool mayClick;

        private void OnEnable()
        {
            GlobalEvents.BowReleased += OnBowReleased;
			
		}

        private void OnDisable()
        {
            GlobalEvents.BowReleased -= OnBowReleased;
			answerController.OnClick -= ClickAnswer;

		}

        public void SetColor(Color color)
        {
			answerTextUI.color = color;
            answerImage.color = color;
		}

        public void SetData(Answer _answer)
        {
            mayClick = true;
            answer = _answer;
            answerInfo = answer.answerInfo;
            isCorrect = answer.correctAnswer;
			answerTextUI.text = answerInfo;
            answerImage.sprite = answer.imageSprite;

            if (answerController.OnClick != ClickAnswer)
			    answerController.OnClick += ClickAnswer;

			if (answer.type == AnswerType.Image)
            {
                answerTextUI.gameObject.SetActive(false);
				answerImage.gameObject.SetActive( true);
			}
			else
			{
                answerTextUI.gameObject.SetActive(true);
				answerImage.gameObject.SetActive( false);
			}
		}

        public void SetSpriteAsset(TMP_SpriteAsset spriteAsset)
        {
			if (answerTextUI.spriteAsset == spriteAsset) return;
			answerTextUI.spriteAsset = spriteAsset;
        }

        public void ClickAnswer()
        {
            if (mayClick)
            {
                GlobalEvents.AnswerDataClicked?.Invoke(this);


            }
            //answerController.OnAnswerClicked();
        }

		private void OnBowReleased(AnswerData data)
		{
            if (data != this) return;
            mayClick = false;
            //answerController.OnClick?.Invoke();
            answerController.OnAnswerClicked();
            gameManager.CheckAnswers(isCorrect);
			
		}
	}
}
