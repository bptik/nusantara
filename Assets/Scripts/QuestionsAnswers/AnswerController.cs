using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Herghys
{
    public class AnswerController : MonoBehaviour
    {
        [SerializeField] SpriteRenderer arrowShot;
        public AnswerData answerData;
        public bool selected;

        public UnityAction OnClick;

        private void Start()
        {
            ResetController();
        }

       /* private void OnEnable()
        {
            OnClick += OnAnswerClicked;
        }

        private void OnDisable()
        {
            OnClick -= OnAnswerClicked;
        }*/

        public void OnAnswerClicked()
        {
            selected = true;
            if(arrowShot != null) arrowShot.gameObject.SetActive(true);
        }

        public void SetArrowSprite(Sprite arrowSprite)
        {
            arrowShot.sprite = arrowSprite;
        }

        public void ResetController()
        {
            selected = false;
            arrowShot.gameObject.SetActive(false);
        }

        private void OnMouseDown()
        {
            OnClick?.Invoke();
        }
    }
}
