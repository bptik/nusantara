using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Herghys
{
    [Serializable]
    public class Answer
    {
        public AnswerType type = AnswerType.Text;
        public string answerInfo;
        public Sprite imageSprite;
        //public TMP_SpriteAsset spriteAsset;
        public bool correctAnswer;
    }
}