using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class QuestionData : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI questionTex;
        [SerializeField] ScrollRect scroll;

        private void Awake()
        {
            if (!scroll) scroll = GetComponent<ScrollRect>();
			questionTex.margin = new Vector4(10,10,10,10);
		}


        public void SetQuestionData(Question question, TMP_SpriteAsset spriteAsset = null)
        {
            questionTex.spriteAsset = spriteAsset;
            if (question.spriteAsset is not null || question.questionInfo.Contains("sprite"))
            {
                questionTex.margin = new Vector4(10, 50, 10, 50);
                questionTex.lineSpacing = question.lineSpacing;
            }
            else
            {
				questionTex.margin = new Vector4(10, 10, 10, 10);
				questionTex.lineSpacing = 10;
            }

            questionTex.text = question.questionInfo;
            scroll.DOVerticalNormalizedPos(1, 0.5f);
		}
    }
}
