using System;
using Herghys.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public class MissionSelectButton : MonoBehaviour
    {
        [SerializeField] int missionNumber;
		[SerializeField] bool requireStar;
		[SerializeField] Button button;
        [SerializeField] MissionSelectionManager manager;
        [SerializeField] string levelName;

        [SerializeField] bool canSelectCharacter = true;
        [SerializeField] bool canSelectWeapon = true;

        private void Awake()
        {
            SetInitialData();
		}

        public void SetInitialData()
        {
			if (!button) button = GetComponent<Button>();
			if (!manager) manager = FindObjectOfType<MissionSelectionManager>();

			requireStar = bool.Parse(PlayerPrefs.GetString(GlobalData.StarRequirementID(missionNumber), "False"));

			if (missionNumber > PlayerPrefs.GetInt(GlobalData.UnlockedLevelID, 1))
				button.interactable = false;
			else
				button.interactable = true;
		}

        public void SetLevel()
        {
            //GlobalData.MissionNumber = missionNumber;
            /*if (missionNumber <= 2)
                levelName = $"{GlobalData.Level}{1}";
            else
				levelName = $"{GlobalData.Level}{2}";*/

			levelName = $"{GlobalData.Level}{missionNumber}";


            manager.SetSelection(canSelectCharacter, canSelectWeapon);
			manager.SetLevelToLoad(missionNumber, levelName, requireStar);

			GlobalEvents.MissionSelected?.Invoke(missionNumber - 1);
        }
    }
}
