using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Herghys
{
	[CreateAssetMenu(fileName = "Character", menuName = "Data/Character")]
	public sealed class Character : ScriptableObject
    {        
		public CharacterData data;
	}

    [Serializable]
    public class CharacterData
    {
        public int characterIndex;
        public string characterName;
        public Sprite characterPortrait;

        [Header("Character")]
        public Sprite headSprite;
        public Sprite bodySprite;
        public Sprite armLeftSprite;
        public Sprite armRightSprite;

	}
}
