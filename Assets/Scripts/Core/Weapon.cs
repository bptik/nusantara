﻿using System;
using UnityEngine;

namespace Herghys
{
	[CreateAssetMenu(fileName = "Weapon", menuName = "Data/Weapon")]
	public sealed class Weapon : ScriptableObject
	{
		public WeaponData weaponData;
	}

	[Serializable]
	public class WeaponData
	{
		public string weaponName;
		public int weaponIndex;
		public Sprite weaponPortrait;

		public Sprite weaponNormal;
		public Sprite weaponAim;
		public Sprite projectile;
		public Sprite projectileCut;
	}
}
