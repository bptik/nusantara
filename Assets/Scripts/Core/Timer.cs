using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class Timer : MonoBehaviour
    {
        [SerializeField] private float time;
        [SerializeField] private float currentTime;
        [SerializeField] private Slider slider;
        [SerializeField] private Image timerFill;
        [SerializeField] private TextMeshProUGUI timerText;
        //[SerializeField] GameState gameState;
        public GameManager manager;

        private IEnumerator IE_Timer;
		float minutes, seconds;

		public void SetTimer(float time)
        {
            this.time   = time;
            currentTime = time;

			slider.value = currentTime / time;
            SetText(currentTime);
		}

        public void StartTimer()
        {
            IE_Timer = IE_StartTimer();
            StartCoroutine(IE_Timer);
            //StartCoroutine(IE_StartTimerMiliseconds());
        }

        public void StopTimer()
        {
            StopCoroutine(IE_Timer);
            //StopAllCoroutines();
        }

        IEnumerator IE_StartTimer()
        {
           
            while (manager.gameState == GameState.Play)
            {
                currentTime -= Time.deltaTime;

				slider.normalizedValue = currentTime / time;

				SetText(currentTime);

                if (currentTime <= 10)
                    timerFill.color = Color.yellow;

				if (currentTime <= 5 )
                    timerFill.color = Color.red;

                if (currentTime <= 0)
                {
                    StopTimer();
                    manager.Finish(false);
                    break;
                }
                //yield return new WaitForSeconds(1);
                yield return null;
            }
        }

		void SetText(int time)
		{
			minutes = Mathf.FloorToInt(time / 60);
			seconds = Mathf.FloorToInt(time % 60);
			timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
		}

		void SetText(float time)
		{
			minutes = Mathf.FloorToInt(time / 60);
			seconds = Mathf.FloorToInt(time % 60);
			timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
		}
	}
}
