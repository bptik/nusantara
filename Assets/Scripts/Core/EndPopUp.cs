using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Herghys
{
    public class EndPopUp : PopUp
    {
        [SerializeField] int starCount;
        [SerializeField] int stickCount;
        [SerializeField] int totalScore;

        [SerializeField] TextMeshProUGUI starText;
        [SerializeField] TextMeshProUGUI stickText;
        [SerializeField] TextMeshProUGUI totalScoreTxt;

        public void SetStickAndStar(int stick, int star, int totalScore)
        {
            starCount = star;
            stickCount = stick;
            this.totalScore = totalScore;

            totalScoreTxt.text = $"Skor: {this.totalScore}";
            starText.text = $"x{starCount}";
            stickText.text = $"x{stickCount}";
        }
    }
}
