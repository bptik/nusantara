using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Herghys
{
    public class AnswerArea : MonoBehaviour
    {
        [SerializeField] Color textColor = Color.white;
        [SerializeField] AnswerData answerData;
        public AnswerController answerController;

        public void SetText(AnswerData data)
        {
            answerData = data;
            answerData.SetColor(textColor);
            answerData.answerController = answerController;
            answerController.answerData = data;
        }
    }
}
