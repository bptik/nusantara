using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Herghys
{
    public sealed class Scoring : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI scoreText;
        [SerializeField] int score;

        public void SetScore(int score)
        {
            this.score = score;
            scoreText.text = $"SCORE: {score}";
        }
    }
}
