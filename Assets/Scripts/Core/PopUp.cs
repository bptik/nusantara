using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public class PopUp : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI tmpText;
        public Button startButton;
        public void SetUpText(string text)
        {
            tmpText.text = text;
        }
    }
}
