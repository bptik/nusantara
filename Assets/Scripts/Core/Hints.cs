using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Herghys
{
    public sealed class Hints : MonoBehaviour
    {
        public GameManager manager;
        public Image notif;
        private void Awake()
        {
            if (!manager) manager = FindObjectOfType<GameManager>();

        }

        private void Start()
        {
            if (manager.TotalSticks > 0) notif.gameObject.SetActive(true);
            else  notif.gameObject.SetActive(false);
        }

        public void ClickHints()
        {
            if (manager.TotalSticks > 0)
                manager.ShowAnswerHints();

            if (manager.TotalSticks <= 0)
            {
                notif.gameObject.SetActive(false);
            }
        }
    }
}
