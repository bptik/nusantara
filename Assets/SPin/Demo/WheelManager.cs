﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WheelManager : MonoBehaviour {

    //Creates the wheel
    SpinWheel wheel = new SpinWheel(10);
    int money = 0;
    int gold = 0;
    public Text text;
    public GameObject go;
    // public GameObject win;
    // public Text winT;

    [Header("PanelSoal")]
    public GameObject PanelSoal1;
    public GameObject PanelSoal2;
    public GameObject PanelSoal3;
    public GameObject PanelSoal4;
    public GameObject PanelSoal5;
    public GameObject PanelSoal6;
    public GameObject PanelSoal7;
    public GameObject PanelSoal8;
    public GameObject PanelSoal9;
    public GameObject PanelSoal10;

	void Start () {
        //Keep track of the player money
        money = PlayerPrefs.GetInt("money", 0);
        UpdateText();

        //Sets the gameobject
        wheel.setWheel(gameObject);

        //Sets the callback
        wheel.AddCallback((index) => {
            switch (index)
            {
                case 1:
                    gold += 750;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "750";
                    PanelSoal1.SetActive(true);
                    break;
                case 2:
                    money += 500;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "500";
                    PanelSoal2.SetActive(true);
                    break;
                case 3:
                    money += 250;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "250";
                    PanelSoal3.SetActive(true);
                    break;
                case 4:
                    money += 1000;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "1000";
                    PanelSoal4.SetActive(true);
                    break;
                case 5:
                    money += 750;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "750";
                    PanelSoal5.SetActive(true);
                    break;
                case 6:
                    money += 250;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "250";
                    PanelSoal6.SetActive(true);
                    break;
                case 7:
                    money += 250;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "250";
                    PanelSoal7.SetActive(true);
                    break;
                case 8:
                    money += 250;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "250";
                    PanelSoal8.SetActive(true);
                    break;
                case 9:
                    money += 500;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "500";
                    PanelSoal9.SetActive(true);
                    break;
                case 10:
                    money += 500;
                    PlayerPrefs.SetInt("Gold4", money);
                    //PlayerPrefs.SetInt("Skor", Score);
                    // win.SetActive(true);
                    // winT.text = "500";
                    PanelSoal10.SetActive(true);
                    break;
            }
            UpdateText();
        });
	}

    public void UpdateText()
    {
        text.text = money + "";
    }

    // public void OkWin()
    // {
    //     win.SetActive(false);
    // }

    public void Spin()
    {
        if (money >= 0)
        {
            money -= 0;
            PlayerPrefs.SetInt("money", money);

            StartCoroutine(wheel.StartNewRun());
            UpdateText();
        } else
        {
            go.SetActive(true);
        }
    }

    public void ok()
    {
        go.SetActive(false);
    }
}
